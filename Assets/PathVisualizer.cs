﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
public class PathVisualizer : MonoBehaviour
{
    public AugmentedImage Image;

    public GameObject centerObject;
    public GameObject goalObject;
    public GameObject pathBall;
    public SettingsController settings;

    private List<GameObject> pathBalls = new List<GameObject>();

    public void Start()
    {
        settings = (SettingsController) FindObjectOfType(typeof(SettingsController));
    }

    public void Update()
    {
        if (Image == null || Image.TrackingState != TrackingState.Tracking)
        {
            centerObject.SetActive(false);
            goalObject.SetActive(false);
            return;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.Default;

        float startSpeed = settings.getStartVelocity();
        float startAngle = settings.getStartAngle();
       
        float mass = settings.getMass();

        float g = settings.getG();
        float c = settings.getAirResistanceC();
        float density = settings.getDensity();
        float area = settings.getArea();


        float startSpeedX = startSpeed * Mathf.Cos(startAngle * (Mathf.PI / 180));
        float startSpeedY = startSpeed * Mathf.Sin(startAngle * (Mathf.PI / 180));

        


        int i = 0;
        foreach(GameObject gameO in pathBalls)
        {
            gameO.SetActive(false);
        }

        float time = 0f;
        float timeDelta = 0.01f;
        float yPos = 0f;
        float xPos = 0f;
        float xSpeed = startSpeedX;
        float ySpeed = startSpeedY;


        while (true)
        {

            xPos = xPos + xSpeed * timeDelta;
            yPos = yPos + ySpeed * timeDelta;

            float airResistance = 0.5f * c * density * area * (xSpeed + ySpeed) * (xSpeed + ySpeed);

            float nice = xSpeed / (xSpeed + ySpeed);

            float accelerationX = (-airResistance * nice) / mass;
            float accelerationY = (-airResistance * (1-nice) - g*mass) / mass;

            xSpeed = xSpeed + accelerationX * timeDelta;
            ySpeed = ySpeed + accelerationY * timeDelta;


            if (time > 10f) break;
            if(pathBalls.Count <= i)
            {
                pathBalls.Add(Instantiate(pathBall, gameObject.transform));
            }
            pathBalls[i].transform.localPosition = new Vector3(xPos, yPos, 0);
            pathBalls[i].SetActive(true);
            if (Frame.Raycast(gameObject.transform.position + xPos * Vector3.right + yPos * Vector3.up, Vector3.down, out hit, 5, raycastFilter))
            {
                if(hit.Distance >= 0f && hit.Distance < 0.15f && i > 5)
                {
                    goalObject.transform.localPosition = hit.Pose.position - gameObject.transform.position;
                    break;
                }
                
            }
            time = time + timeDelta;
            i = i + 1;
        }
        
        if(goalObject.transform.localPosition.x == 0 && goalObject.transform.localPosition.z == 0)
        {
            goalObject.SetActive(false);
        }
        else
        {
            goalObject.SetActive(true);
        }
        
        
    }
}
