﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

public class SceneController : MonoBehaviour
{

    public PathVisualizer pathVisualizer;

    private PathVisualizer visualizer = null;

    private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();

    private Anchor anchor;

    private void ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity =
            unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject =
                    toastClass.CallStatic<AndroidJavaObject>(
                        "makeText", unityActivity, message, 0);
                toastObject.Call("show");
            }));
        }
    }

    void QuitOnConnectionErrors()
    {
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            ShowAndroidToastMessage("Kannattaa antaa se kameraoikeus :D");
        }
        else if (Session.Status.IsError())
        {
            ShowAndroidToastMessage("Jotain meni pieleen... Ei voi mitää :/");
        }
    }

    void Awake()
    {
        Application.targetFrameRate = 60;
    }

    void Update()
    {
        QuitOnConnectionErrors();

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }


        if (Session.Status != SessionStatus.Tracking)
        {
            Screen.sleepTimeout = SleepTimeout.SystemSetting;
            return;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        Session.GetTrackables<AugmentedImage>(
                m_TempAugmentedImages, TrackableQueryFilter.Updated);

        foreach (var image in m_TempAugmentedImages)
        {
            
            if (image.TrackingState == TrackingState.Tracking)
            {
                if(visualizer == null)
                {
                    anchor = image.CreateAnchor(image.CenterPose);
                    visualizer = (PathVisualizer)Instantiate(
                        pathVisualizer, anchor.transform);
                    
                }

                visualizer.transform.eulerAngles = new Vector3(0, anchor.transform.eulerAngles.y, 0);
                visualizer.transform.position = anchor.transform.position;
                visualizer.Image = image;
                
            }
            else if (image.TrackingState == TrackingState.Stopped && visualizer != null)
            {
                GameObject.Destroy(visualizer.gameObject);
                visualizer = null;
            }
        }


    }
}
