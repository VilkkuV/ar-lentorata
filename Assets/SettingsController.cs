﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    float mass;
    float startVelocity;
    float startAngle;
    float airResistanceC;
    float area;
    float density;
    float g;

    public InputField massField;
    public InputField startVelocityField;
    public InputField startAngleField;
    public InputField airResistanceCField;
    public InputField areaField;
    public InputField densityField;
    public InputField gField;

    void Start()
    {
        mass = PlayerPrefs.GetFloat("mass", 0.05f);
        startVelocity = PlayerPrefs.GetFloat("startVelocity", 3f);
        startAngle = PlayerPrefs.GetFloat("startAngle", 0f);
        airResistanceC = PlayerPrefs.GetFloat("airResistanceC", 0.5f);
        area = PlayerPrefs.GetFloat("area", Mathf.PI * 0.03f * 0.03f);
        density = PlayerPrefs.GetFloat("density", 1.225f);
        g = PlayerPrefs.GetFloat("g", 9.81f);

        massField.text = mass.ToString();
        startVelocityField.text = startVelocity.ToString();
        startAngleField.text = startAngle.ToString();
        airResistanceCField.text = airResistanceC.ToString();
        areaField.text = area.ToString();
        densityField.text = density.ToString();
        gField.text = g.ToString();
    }

    public float getMass()
    {
        return mass;
    }
    public float getStartVelocity()
    {
        return startVelocity;
    }
    public float getStartAngle()
    {
        return startAngle;
    }
    public float getAirResistanceC()
    {
        return airResistanceC;
    }
    public float getArea()
    {
        return area;
    }
    public float getDensity()
    {
        return density;
    }
    public float getG()
    {
        return g;
    }

    public void UpdateMass(string newMass)
    {
        newMass = newMass.Replace(',', '.');
        mass = float.Parse(newMass, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("mass", mass);
    }

    public void UpdateStartVelocity(string newVel)
    {
        newVel = newVel.Replace(',', '.');
        startVelocity = float.Parse(newVel, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("startVelocity", startVelocity);
    }

    public void UpdateStartAngle(string newAngle)
    {
        newAngle = newAngle.Replace(',', '.');
        startAngle = float.Parse(newAngle, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("startAngle", startAngle);
    }

    public void UpdateResistanceC(string newResistance)
    {
        newResistance = newResistance.Replace(',', '.');
        airResistanceC = float.Parse(newResistance, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("airResistanceC", airResistanceC);
    }

    public void UpdateArea(string newArea)
    {
        newArea = newArea.Replace(',', '.');
        area = float.Parse(newArea, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("area", area);
    }

    public void UpdateDensity(string newDensity)
    {
        newDensity = newDensity.Replace(',', '.');
        density = float.Parse(newDensity, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("density", density);
    }

    public void UpdateG(string newG)
    {
        newG = newG.Replace(',', '.');
        g = float.Parse(newG, System.Globalization.CultureInfo.InvariantCulture);
        PlayerPrefs.SetFloat("g", g);
    }

    public void savePrefs()
    {
        PlayerPrefs.Save();
    }

    public void Reset()
    {
        PlayerPrefs.DeleteAll();
        Start();
    }
}
